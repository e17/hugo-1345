---
title: "Hello"
date: 2019-10-07T22:47:51+01:00
draft: false
---

I'm Ed / Eddie / Edward (too many names... choose whichever you prefer)! I have broad interests in technology, business, society and meditation.

I'm currently doing a course in fullstack web development at [Wild Code School](https://www.wildcodeschool.com) in Lisbon. While my web development skills are very much WIP, I have strong experience in business development and producing fundraising materials for growth companies - do not hesitate to get in touch if you are looking for help in these areas!

You can also find me here:

- [LinkedIn](https://www.linkedin.com/in/edward-watson/)
- [Twitter - @EdwardWatson](https://twitter.com/edwardwatson)
- Or, or you can email me at e (at) (myfirstname&lastname).com
