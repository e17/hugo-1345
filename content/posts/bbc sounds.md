---
title: "BBC Sounds is a terrible app"
date: 2019-10-01T22:47:51+01:00
draft: true
author: Edward

---

In October 2018, BBC Sounds website and app was launched, intended to replaced BBC iPlayer Radio.

BBC iPlayer Radio was a great app, in terms of usability and providing access to radio archives online. It allowed users to search through the archive, both in terms of show and the radio station it was aired. It displayed historic programming schedules and allowed you to dive into shows one by one bsed on this. However, presumably, it had a large legacy codebase that was proving troublesome to maintain.

On top of that, podcasts are increasing in popularity and many radio shows are syndicated as podcasts. Radio stations also seek to capture youth audiences.

As such, BBC Sounds combines radio shows and podcasts as one and does so in the form of a typical podcast style, app, with downloads, bookmarked shows etc. 

To give you a general flavour of the BBC Sounds app, check out this app store review:

![app store review](https://res.cloudinary.com/edwardwatson/image/upload/c_scale,w_476/v1570548626/ew/appstorereview_t2xrq7.jpg)

For a while users were given the option of which to choose, however as of September 2019, BBC iPlayer Radio was decommissioned. 

This is troublesome, as all users are forced to use BBC Sounds - which doesn't speak to how listeners enjoy radio.

# What's wrong with BBC Sounds?

By conflating Radio Shows with Podcasts, a crucial difference between radio and podcasts is skirted over:

- Podcasts are typically enjoyed on demand - listeners choose exactly what they want to listen to

- Radio is enjoyed passively i.e. we listen to whatever has been programmed! In enjoying passively, we come to appreciate programming schedules and incorporate these into our daily rituals:

## Programming schedules have value

Programming is deeply linked to time of day and our personal schedule. It is also intentional - the order of programming has value in the same way that tracks on an LP are sequenced to tell a story or hook in the listener. To listen to the songs of Sgt. Pepper on shuffle loses something, as listening to Radio 4 on shuffle loses something.

## Daily rituals

I like to listen to Radio 4 regardless of what's on. In the morning I regularly listening to Woman's Hour as I start my working day and enjoy being led onto whatever is scheduled for the day. I learn about new things and enjoy shows I wouldn't pick out on an on-demand service. Others tune in for their favourite Breakfast or News Show, where current events frame their day.

Users should able to perform these rituals with shows played in the past. Ideally, by relistening to a previous day's radio schedule, as it was broadcast, in order.

This is lost with BBC Sounds!

BBC iPlayer Radio had a sort of a v1 of this feature by listening shows as they were broadcast. For BBC Sounds, I would have to manually look up the timetable elsewhere, search for each show individually (they are not sorted by Radio Station, only by category) and manually queue them.

## A better service

BBC would be wise to completely seperate archived Radio listening and podcast apps. While the content is the same, listening behaviour and user needs are very different!






