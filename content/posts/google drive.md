---
title: "Using Google Drive for music streaming to mobile"
date: 2019-10-07T22:47:51+01:00
draft: true
author: Edward

---

I find myself with a large offline music collection. 80% of the music I listen to, I have from ripped CDs and other sources, so it doesn't make sense to pay for Spotify. 

## Backing up to the cloud with Google Drive

While Google Drive offers 15gb of free storage as minimum, [Google One](https://one.google.com/about), offers 100gb.

I found myself using [G Suite] (https://gsuite.google.co.uk/intl/en_uk/pricing.html)for my personal email administration and storage and was amazed at its unlimited cloud storage option.

I set about trying to make best use of this unlimited storage for my music collection.

## Playing directly from Google Drive

This is really bad - while you can play individual tracks there are no in-built tools for streaming music. Google does not encourage users to do this.

## Google File Stream

Download [Google Backup & Sync or Drive File Stream](https://support.google.com/drive/answer/7638428?hl=en) for Mac/Windows desktop, which will allow you to stream files directly to your PC (no mobile option).

## Kodi / Google Drive Plugin

The [Google Drive plugin](https://kodi.wiki/view/Add-onGoogle_Drive) in Kodi's official add-on repo lets you stream from Google Drive, including video and music. This works on desktop and mobile.

You can achieve something siilar with [RClone and Plex](https://forum.rclone.org/t/my-recommended-google-drive-and-plex-mount-settings-updated-6-mar-2019/6132).

While this usually works, I have experienced some slow speeds streaming directly from Google Drive.

## Best option - Stream on Mobile via Google Play Music

Google Play Music allows you to upload up to 50,000 of your own songs. 

After installing Back Up & Sync or File Stream point Google Play Music to sync directly with the a music folder in your Google Drive.

50,000 may not be your entire library, but I have a 'favourites' folder which I sync only. So I stream a revolving 50,000 of my total with Play Music, which easily offers variety.


