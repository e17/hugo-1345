---
title: "Crowman"
date: 2019-10-09T07:47:51+01:00
draft: true
author: Edward

---

A fascinating thing about office work is meeting people you wouldn't ordinarily meet. Most of us operate in narrow circles with friends who share our values or a chapter of life. At work, you become across people who are neither and become intimately aquainted with lives at odds with your own.

One character that sticks out was Crowman. His name was probably David or Martin, but to be honest, his actual name escaped me after the crows arrived.

I encountered Crowman working in a corporate office for a bank, which was my first job out of university. We were both in the open office, with adjacent desks fortunately backing onto full-height glass windows. We were seperated by cubicles in seperate arrays of desks, arranged in a way, that while being incredibly close to each other, we could only ever see the tops of each others head so weren't compelled to chat.

I was happy to be in an open office where I could overhear conversations about goings on and learn my trade - before this, I knew what a bank did, but had no idea of the sorts of conversations people actually have when operating in a bank. 

Crowman was not so happy to be in an open office - he was twice my age and senior enough to be given an important title, but not senior to have his own office. He was a victim of open plan, being constantly unsettled by the constant flow of people to his desk asking for things.

Our area of the department made sure the bank held enough financial reserves to cover its retail lending and investment trading operations to stay afloat should the market crash. This was only four years out from the crisis, where Crowman himself had been laid off from Lehman Brothers. So had an intimate understanding of the risks involved and the need for a margin of error. He was a true know it all, in that he really did seem to know it all about this particular area - a walking dictionary and reference guide to banking regulation.

There was a sadness to Crowman. He would arrive early - at least before 8AM daily, and always leave later than me, past 6.30PM. He had a mountain of work owing to his encylopedic knowledge - everyone wanting his help, advice or rubber stamp for their work. He would constantly huff and puff about his workload, and told all those who came to his desk about his extreme busyness, of which they would take no notice. 

While Crowman did not seem to enjoy work, I had a sense he once did. When speaking about the sector, conceptual topics or regulation, he glowed with enthusiasm - but almost out of habit, muscle memory from indulging in what was once a passion.

He spent the majority of his 50+ hour weeks battling against cumbersome Excel sheets or putting said Excel Sheets into slide decks. He would huff and puff through this mountain of work, barely looking up and working through his lunch hour with a pre-prepared soundwich. I was acutely aware of exactly what he was working on at any time, as he would narrate what he was doing for all to know he was the busiest, most inundated person on the floor and we should all give him a break.

Crowman's outlook seemed to be darker with every day - on a good day, this huffing and puffing was a simple a reaction to his workload, but on a bad day, seemed to be a symptom of him struggling with life itself.

For the first 6 months or so I was there over Winter, Crowman only spoke about work, albeit letting slip one glimpse of his personal life one Friday afternoon, when the floor was more chatty. When asked how he met his current partner, he noted his ex-wife and mother of his children, had up and left one morning, some years ago. He did not know why she had left and still doesn't to this day.

I found this especially sad because he'd done everything he thought was right in life - given wholly to his career, been passionate about his work, and married and had children. And yet the world had delivered him a miserable lot. Well, at least until the crows arrived.

One Sping morning, I arrived to find Crowman staring intensely out the window directly next to his desk. Against all odds, a family of crows had nested high above in this metal and glass building. London has great flora and fauna, so there is more wildlife than you might expect, but it seemed unlikely that the crows should nest 100M high in a metal and glass building.

Crowman's work quickly took a backseat as he began to celebrate the greatness of crows - "How rare that they should nest here!", "Did you know that owls and crows hate each other by instinct?", "Crows have the ability to recognise human faces... I wonder whether they recognise me?".

He was ignited and bursting with life. A new man, he excitedly brought his digital camera the office to begin documenting the crows. I imagined a spare room at home repurposed into a crow documentation room. He took regular photos of their nest and tracked their movements with glee, probably logging it into an Excel sheet for furhter analysis.

When colleagues came to visit his desk to ask about his work, instead of sighing, the new Crowman took great pleasure to introduce them to the crows and their behaviours - he took great pride in now being a crow expert.

More unlikely than the crows nesting in such an unlikely place, was that they had done so close to the man who needed it most. Senior leaders would drop by his desk to discuss important matters and would be treated to a symphony of crow worship, with a comprehensive explanation of Crowman's personal aviary. For a brief moment, their running a major international bank was put on a hold while they listened to the ins and outs of crow mating behaviours, and how they were apparently as intelligent as a human seven year old.

Surely, without Crowman's delight, no-one would have taken any interest in the crows and the nest would have been dutifully swept away during window cleaning. But Crowman spent one morning on his phone tracking down the window cleaning subcontracting company to brief them on the crows. "They are nesting for at least nine weeks and it's crucial their nest is not disturbed!". And the nest was not disturbed!

This sort of thing continued for the next two months, although I'm not really sure what happened after to Crowman or the crow - I left that job half way through the nesting period. I can only imagine that by now, he has quit banking and founded a crow sanctuary.







