---
title: "Every dog I've ever photographed"
date: 2019-10-01T22:47:51+01:00
draft: true
author: Edward

---

Google's image classification is really good. It uses [machine learning and convolutional neural networks] (https://developers.google.com/machine-learning/practica/image-classification). I haven't taken time to understand how this works. But I do know whatever I can type into Google Photos apart from 'penis' is returned. Usually in great number, as I've been syncing all my mobile photos to Google photos since 2014.

The only practical application for me is recalling pictures of receipts for expenses. I take a photo of every receipt for my expenses and Google Photos returns all receipts reliably. 

Apart from this, it's a novelty, to see what it can or can't categorise, and to view your photo archive though an idosyncratic lens, e.g. here are some chairs I've photographed over recent months:

![pictures of chairs from Google images](https://res.cloudinary.com/edwardwatson/image/upload/c_scale,w_452/v1570567667/Every%20Dog/chairs_vaudyf.png)

Although, looking at pictures of dogs does bring me pleasure. Particarly, dogs I have met.

So without further ado, for your pleasure, please find all the dogs I've taken photos of with an Android phone:
 
#### A big white fluffy dog I met in Clissold Park, Stoke Newington: 

![big white fully dog](https://res.cloudinary.com/edwardwatson/image/upload/c_scale,h_500/v1570567872/Every%20Dog/Unknown_Clissold_Park_jhkd3z.jpg)

#### A puppy who lives on a canal boat: 

![puppy with cat on canal boat](https://res.cloudinary.com/edwardwatson/image/upload/c_scale,h_500/v1570567980/Every%20Dog/Untitled_jyxjtf.png)

#### Tiff, who I looked after for a couple of weeks on a TrustedHouseSitters assignment:

![puppy with cat on canal boat](https://res.cloudinary.com/edwardwatson/image/upload/c_scale,h_500/v1570568026/Every%20Dog/Tiff_n75nfr.jpg)

#### Supergirl, a colleague's dog:

![bedlington terrier in office](https://res.cloudinary.com/edwardwatson/image/upload/c_scale,h_500/v1570568173/Every%20Dog/Supergirl_kor7eo.png)

#### Reggie, my cousin's dog:

![cute chocolate labrador puppy](https://res.cloudinary.com/edwardwatson/image/upload/v1570568229/Every%20Dog/reggie_bwu3ys.png)

##### Lionel, another TrustedHouseSitters assignment dog. He was half Standard Poodle and half Newfoundland. Easily, the biggest dog most people have ever seen! A handful at times:

![big newfoundland x poodle](https://res.cloudinary.com/edwardwatson/image/upload/c_scale,h_500/v1570568386/Every%20Dog/Lionel_bxecon.jpg)

#### Basil, my brother's cockapoo:

![cockapoo in car](https://res.cloudinary.com/edwardwatson/image/upload/c_scale,h_500/v1570568421/Every%20Dog/KT85TquhvERKaY6glnm1nx39BpMAV2TerKnA1iDK_js08MrAIZcfwJ9bgOiof0yBnsxX2KGYzFiXh6l_JiFSb2lktt1Lpu23hoCpufjVkTNtw0QzkjZO2fBKM9NtzAUnd_Il9DQIFbkz5B2Gd6272_sBbji95FPXaUgZC5XiLzENLH4BJlp5J1P0njQ6dU68PF_ZykIodfF6hWl_ajqzpw.jpg)

#### Ella, the labrador. A dog my parents were looking after... also for TrustedHouseSitters!:

![labrador](https://res.cloudinary.com/edwardwatson/image/upload/c_scale,h_500/v1570568565/Every%20Dog/IMG_20180216_170152_dkhe6k.jpg)

And that's it... for now!

Re: All the TrustedHouseSitters dogs, TrustedHouseSitters is a pet and housesitting exchange website i.e. you look after people's pets and houses while they go on holiday. You stay for free in a (hopefully) interesting location or nice house. The host gets free pet care and someone to watch over their house and water the plants!

[You can learn more sign up for the service here. Using this affiliate link, you get 25% off - usually £89 a year. (I also get a discount on the service when you do this.)](https://www.trustedhousesitters.com/refer/raf267229/)



