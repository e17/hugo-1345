---
title: "Accepting confusion is a crucial part of learning"
date: 2019-10-21T05:47:51+01:00
draft: true
author: Edward
---

I'm currently in Lisbon until Spring next year, learning two really hard things: Portuguese and web development languages (namely Javascript and specifically React). Each I had a sort of basis for before - having learnt Spanish and done basic web development, and some SAS quantitative analysis before. But it's hard.

Portuguese is hard because of the pronounciation. Spanish is phonetic so you can easily correlate the written word with the spoken word. This is helpful for finding shared vocabularies with English usually with latin routes. This is not the case for Portuguese!

Part of the fear I experienced when I learnt new languages was the fact that I couldn't rely on book learning. Other subjects - Maths, English, science etc. I have succeeded on book learning alone. I'm terrible at paying attention to lectures/teaching for a long period of time so it was the only way.

But with languages no! You have to piece concepts, syntax and nuanced irregularities together, bit by bit, becoming familiar with listening, many exceptions to rules and hearing things you simply don't understand because they contain concepts more advanced than your level. Through this you have to develop an acceptance that you don't understand everything - and even if you put in 8, 12 hour days, you still wouldn't for a long time.

This acceptance that you don't understand - and won't understand - is really important when learning complex things. Because learning isn't linear, each hour you put in doesn't yield an extra unit of learning. Rather you put in hour after hour, building context and familiarity, and understanding jumps and starts, as you connect previously disparate concepts.

When you don't understand something, it feels as though you can't connect the concept to something else in your arsenal. As we learn through connection - e.g. when learning division in Maths, we can understand it through multiplication i.e. 30/5 = 12, as 12x5 = 30.

This forming of connection takes time, and building context around the new concept you're learning. And also, just familiarity with the topic. When we talk about 'sleeping on something' to solve difficult problems, it does really work and is an important component of learning and forming memories and so knowledge.

I took part as a research participant on the effects of sleep on problem solving at Lancaster University. I was invited to solve a range of easy to difficult puzzles, some of which I could solve, many of which I couldn't. I then went home and returned to the study in the morning, whereby I could solve more puzzles than the previous day. I had had more time, sure, but those whose time to think involved sleeping tended to do better, than those that didn't.[You can read the study here. ](https://www.lancaster.ac.uk/staff/monaghan/papers/sio_monaghan_ormerod_12_memcog.pdf)

With learning Javascript and React, I feel that while there are many things I don't understand right now and I continue to note down and interrogate these areas of little understanding - I must accept that my understanding will advance in jump and starts.

By moving onto new topics before mastering previous ones, I increase the context in the language and give my brain chances to make connections between the concepts that make sense, and those that don't. Aha! moments are typically due to your connecting the sum of the parts, which lets you understand how each consistuent part works by connecting it with things you already understand.

This for me in Javascript was understanding data structures... alone they make no sense, but when they are used with functions, they have purpose and connection to other things, and the logic made sense. (Apologies in advance if you're reading this and have no idea what I'm talking about re: Javascript). In spoken language, for me, I found using language together to achieve real things and access experiences in Spanish gave langugae emotional resonance. By connecting the language, with my emotional state, something I am very familiar with, many parts of the language clicked into place.

For me, when applied to learning how to learn, the key message is to accept confusion and lack of understanding, and keep powering on with deep curiosity into meta topics - how things fit together, and how when component parts are connected, it relates to the real world - to the market, to your emotions, to other people, whatever. These things we understand and are familiar with so are easy to make new connections with. But before Aha moments, you have just accept confusion!
